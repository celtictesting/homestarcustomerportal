﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarCP.URLA.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Click_Employer_Details_Continue_Button recording.
    /// </summary>
    [TestModule("9c6e2be2-9816-4f40-9567-dae99fd45f40", ModuleType.Recording, 1)]
    public partial class Click_Employer_Details_Continue_Button : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarCP.HomestarCPRepository repository.
        /// </summary>
        public static global::HomestarCP.HomestarCPRepository repo = global::HomestarCP.HomestarCPRepository.Instance;

        static Click_Employer_Details_Continue_Button instance = new Click_Employer_Details_Continue_Button();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Click_Employer_Details_Continue_Button()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Click_Employer_Details_Continue_Button Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Wait", "Waiting 1m to exist. Associated repository item: 'Homestar.Continue'", repo.Homestar.ContinueInfo, new ActionTimeout(60000), new RecordItemIndex(0));
            repo.Homestar.ContinueInfo.WaitForExists(60000);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Homestar.Continue' at Center.", repo.Homestar.ContinueInfo, new RecordItemIndex(1));
            repo.Homestar.Continue.Click();
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
