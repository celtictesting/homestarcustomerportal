﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarCP.URLA.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Click_In_Service_Veteran_Status recording.
    /// </summary>
    [TestModule("feab4c2f-abd3-44f7-8ec0-8575188c67d4", ModuleType.Recording, 1)]
    public partial class Click_In_Service_Veteran_Status : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarCP.HomestarCPRepository repository.
        /// </summary>
        public static global::HomestarCP.HomestarCPRepository repo = global::HomestarCP.HomestarCPRepository.Instance;

        static Click_In_Service_Veteran_Status instance = new Click_In_Service_Veteran_Status();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Click_In_Service_Veteran_Status()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Click_In_Service_Veteran_Status Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking WaitForDocumentLoaded() on item 'Homestar.Page_Dom'.", repo.Homestar.Page_DomInfo, new RecordItemIndex(0));
            repo.Homestar.Page_Dom.WaitForDocumentLoaded();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Homestar.In_Service_Button_Veteran_Status' at Center.", repo.Homestar.In_Service_Button_Veteran_StatusInfo, new RecordItemIndex(1));
            repo.Homestar.In_Service_Button_Veteran_Status.Click();
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
