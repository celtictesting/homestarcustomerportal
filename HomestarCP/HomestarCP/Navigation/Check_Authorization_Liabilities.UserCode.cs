﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// Your custom recording code should go in this file.
// The designer will only add methods to this file, so your custom code won't be overwritten.
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace HomestarCP.Navigation
{
    public partial class Check_Authorization_Liabilities
    {
        /// <summary>
        /// This method gets called right after the recording has been started.
        /// It can be used to execute recording specific initialization code.
        /// </summary>
        private void Init()
        {
            // Your recording specific initialization code goes here.
        }

        public void Validate_YourIncomeRecordCouldNotBeFoundP(RepoItemInfo divtagInfo)
        {
//            Report.Log(ReportLevel.Info, "Validation", "Validating Exists on item 'divtagInfo'.", divtagInfo);
//            Validate.Exists(divtagInfo);
            
			if (divtagInfo.Exists(5000))
            {
            	repo.New_Application.Income.Income_Okay_Button.Click();
            	Report.Info("Clicked Okay button");
            	repo.New_Application.Income.IDontWantToAddIncome.Click();
            	Report.Info("Clicked I Don't Want to Add Income button");
            }
            
            else
            {
            	repo.New_Application.Income.Income_Okay_Button.Click();
            	repo.New_Application.Income.Continue_Button_Income.Click();
            	Report.Info("Clicked Income Continue button");
            }
        }

    }
}
