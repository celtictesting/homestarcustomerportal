﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarCP.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The StartNewApplication recording.
    /// </summary>
    [TestModule("f3c09da0-45e1-40a3-bb2f-db08cbd1a4ef", ModuleType.Recording, 1)]
    public partial class StartNewApplication : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarCP.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarCP.HomestarSalesforceRepository repo = global::HomestarCP.HomestarSalesforceRepository.Instance;

        static StartNewApplication instance = new StartNewApplication();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public StartNewApplication()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static StartNewApplication Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking WaitForDocumentLoaded() on item 'Homestar'.", repo.Homestar.SelfInfo, new RecordItemIndex(0));
            repo.Homestar.Self.WaitForDocumentLoaded();
            Delay.Milliseconds(0);
            
            try {
                Report.Log(ReportLevel.Info, "Mouse", "(Optional Action)\r\nMouse Left Click item 'Homestar.ApplyForANewMortgage' at Center.", repo.Homestar.ApplyForANewMortgageInfo, new RecordItemIndex(1));
                repo.Homestar.ApplyForANewMortgage.Click();
                Delay.Milliseconds(200);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(1)); }
            
            try {
                Report.Log(ReportLevel.Info, "Wait", "(Optional Action)\r\nWaiting 30s to exist. Associated repository item: 'Homestar_Customer_Portal.New_Application.Get_Started.GetStarted_Header'", repo.Homestar_Customer_Portal.New_Application.Get_Started.GetStarted_HeaderInfo, new ActionTimeout(30000), new RecordItemIndex(2));
                repo.Homestar_Customer_Portal.New_Application.Get_Started.GetStarted_HeaderInfo.WaitForExists(30000);
            } catch(Exception ex) { Report.Log(ReportLevel.Warn, "Module", "(Optional Action) " + ex.Message, new RecordItemIndex(2)); }
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
