﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarCP.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Click_Yes_Authorization_for_CoApplicant recording.
    /// </summary>
    [TestModule("0c98d60a-8bfc-4375-9b77-16607d2f69c1", ModuleType.Recording, 1)]
    public partial class Click_Yes_Authorization_for_CoApplicant : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarCP.HomestarUserPortalRepository repository.
        /// </summary>
        public static global::HomestarCP.HomestarUserPortalRepository repo = global::HomestarCP.HomestarUserPortalRepository.Instance;

        static Click_Yes_Authorization_for_CoApplicant instance = new Click_Yes_Authorization_for_CoApplicant();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Click_Yes_Authorization_for_CoApplicant()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Click_Yes_Authorization_for_CoApplicant Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'New_Application.Get_Started.CoApp_Yes_IAm' at Center.", repo.New_Application.Get_Started.CoApp_Yes_IAmInfo, new RecordItemIndex(0));
            repo.New_Application.Get_Started.CoApp_Yes_IAm.Click();
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
