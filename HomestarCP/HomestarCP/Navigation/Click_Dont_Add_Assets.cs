﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarCP.Navigation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Click_Dont_Add_Assets recording.
    /// </summary>
    [TestModule("8f938223-8075-47e9-976b-0f49b81f7c3b", ModuleType.Recording, 1)]
    public partial class Click_Dont_Add_Assets : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarCP.HomestarUserPortalRepository repository.
        /// </summary>
        public static global::HomestarCP.HomestarUserPortalRepository repo = global::HomestarCP.HomestarUserPortalRepository.Instance;

        static Click_Dont_Add_Assets instance = new Click_Dont_Add_Assets();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Click_Dont_Add_Assets()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Click_Dont_Add_Assets Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking WaitForDocumentLoaded() on item 'New_Application.Page_Dom'.", repo.New_Application.Page_DomInfo, new RecordItemIndex(0));
            repo.New_Application.Page_Dom.WaitForDocumentLoaded();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Wait", "Waiting 30s to exist. Associated repository item: 'New_Application.Assets.No_I_Do_Not_Want_To_Add_Assets_Button'", repo.New_Application.Assets.No_I_Do_Not_Want_To_Add_Assets_ButtonInfo, new ActionTimeout(30000), new RecordItemIndex(1));
            repo.New_Application.Assets.No_I_Do_Not_Want_To_Add_Assets_ButtonInfo.WaitForExists(30000);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'New_Application.Assets.No_I_Do_Not_Want_To_Add_Assets_Button' at Center.", repo.New_Application.Assets.No_I_Do_Not_Want_To_Add_Assets_ButtonInfo, new RecordItemIndex(2));
            repo.New_Application.Assets.No_I_Do_Not_Want_To_Add_Assets_Button.Click();
            Delay.Milliseconds(200);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
