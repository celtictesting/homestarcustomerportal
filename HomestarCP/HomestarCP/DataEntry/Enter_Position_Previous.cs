﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarCP.DataEntry
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Enter_Position_Previous recording.
    /// </summary>
    [TestModule("ca9e5b85-898b-4c35-a00b-2cdb9422d567", ModuleType.Recording, 1)]
    public partial class Enter_Position_Previous : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarCP.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarCP.HomestarSalesforceRepository repo = global::HomestarCP.HomestarSalesforceRepository.Instance;

        static Enter_Position_Previous instance = new Enter_Position_Previous();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Enter_Position_Previous()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Enter_Position_Previous Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Employment_Info.Position_In_Borrower_Previous_Employment' at Center.", repo.Salesforce_Website_Chrome.Employment_Info.Position_In_Borrower_Previous_EmploymentInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Employment_Info.Position_In_Borrower_Previous_Employment.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Value to '' on item 'Salesforce_Website_Chrome.Employment_Info.Position_In_Borrower_Previous_Employment'.", repo.Salesforce_Website_Chrome.Employment_Info.Position_In_Borrower_Previous_EmploymentInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Employment_Info.Position_In_Borrower_Previous_Employment.Element.SetAttributeValue("Value", "");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'Mechaninc' with focus on 'Salesforce_Website_Chrome.Employment_Info.Position_In_Borrower_Previous_Employment'.", repo.Salesforce_Website_Chrome.Employment_Info.Position_In_Borrower_Previous_EmploymentInfo, new RecordItemIndex(2));
            repo.Salesforce_Website_Chrome.Employment_Info.Position_In_Borrower_Previous_Employment.PressKeys("Mechaninc");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
