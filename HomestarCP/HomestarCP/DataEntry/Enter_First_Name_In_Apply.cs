﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarCP.DataEntry
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Enter_First_Name_In_Apply recording.
    /// </summary>
    [TestModule("0ce08869-d023-4381-8f44-e6b78c942861", ModuleType.Recording, 1)]
    public partial class Enter_First_Name_In_Apply : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarCP.HomestarUserPortalRepository repository.
        /// </summary>
        public static global::HomestarCP.HomestarUserPortalRepository repo = global::HomestarCP.HomestarUserPortalRepository.Instance;

        static Enter_First_Name_In_Apply instance = new Enter_First_Name_In_Apply();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Enter_First_Name_In_Apply()
        {
            ProfileFirstName = "";
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Enter_First_Name_In_Apply Instance
        {
            get { return instance; }
        }

#region Variables

        string _ProfileFirstName;

        /// <summary>
        /// Gets or sets the value of variable ProfileFirstName.
        /// </summary>
        [TestVariable("8ddbb429-0fa1-4e69-80ef-4afe69d4ab66")]
        public string ProfileFirstName
        {
            get { return _ProfileFirstName; }
            set { _ProfileFirstName = value; }
        }

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Home_Screen.Apply_Now.First_Name_Field_In_Apply_Now' at Center.", repo.Home_Screen.Apply_Now.First_Name_Field_In_Apply_NowInfo, new RecordItemIndex(0));
            repo.Home_Screen.Apply_Now.First_Name_Field_In_Apply_Now.Click();
            Delay.Milliseconds(0);
            
            //UserCodeMethod(repo.Home_Screen.Apply_Now.First_Name_Field_In_Apply_NowInfo);
            //Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence from variable '$ProfileFirstName' with focus on 'Home_Screen.Apply_Now.First_Name_Field_In_Apply_Now'.", repo.Home_Screen.Apply_Now.First_Name_Field_In_Apply_NowInfo, new RecordItemIndex(2));
            repo.Home_Screen.Apply_Now.First_Name_Field_In_Apply_Now.PressKeys(ProfileFirstName, 20);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "User", ProfileFirstName, new RecordItemIndex(3));
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
