﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// Your custom recording code should go in this file.
// The designer will only add methods to this file, so your custom code won't be overwritten.
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace HomestarCP.DataEntry
{
    public partial class Enter_City_Current_Emp_B
    {
        /// <summary>
        /// This method gets called right after the recording has been started.
        /// It can be used to execute recording specific initialization code.
        /// </summary>
        private void Init()
        {
            // Your recording specific initialization code goes here.
        }

        public void PopulateCityIfEmpty(RepoItemInfo inputtagInfo)
        {
            var city = repo.Salesforce_Website_Chrome.Employment_Info_B_CoB.City_In_Current_Employment_B.TagValue;
            
            if(city == null)
            {
            	inputtagInfo.FindAdapter<InputTag>().PressKeys("KENNESAW");
            }
        }

    }
}
