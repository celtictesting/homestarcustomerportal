﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarCP.DataEntry
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Enter_Make_And_Year_And_Value recording.
    /// </summary>
    [TestModule("110f8ad6-00a9-4f9a-8113-5d9d60363dd9", ModuleType.Recording, 1)]
    public partial class Enter_Make_And_Year_And_Value : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarCP.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarCP.HomestarSalesforceRepository repo = global::HomestarCP.HomestarSalesforceRepository.Instance;

        static Enter_Make_And_Year_And_Value instance = new Enter_Make_And_Year_And_Value();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Enter_Make_And_Year_And_Value()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Enter_Make_And_Year_And_Value Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Invoke action", "Invoking WaitForDocumentLoaded() on item 'Salesforce_Website_Chrome'.", repo.Salesforce_Website_Chrome.SelfInfo, new RecordItemIndex(0));
            repo.Salesforce_Website_Chrome.Self.WaitForDocumentLoaded();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Asset_REO_Liability.Make_And_Year_In_Automobile_Owned' at Center.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.Make_And_Year_In_Automobile_OwnedInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Asset_REO_Liability.Make_And_Year_In_Automobile_Owned.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Value to '' on item 'Salesforce_Website_Chrome.Asset_REO_Liability.Make_And_Year_In_Automobile_Owned'.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.Make_And_Year_In_Automobile_OwnedInfo, new RecordItemIndex(2));
            repo.Salesforce_Website_Chrome.Asset_REO_Liability.Make_And_Year_In_Automobile_Owned.Element.SetAttributeValue("Value", "");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'Honda Civic 2019' with focus on 'Salesforce_Website_Chrome.Asset_REO_Liability.Make_And_Year_In_Automobile_Owned'.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.Make_And_Year_In_Automobile_OwnedInfo, new RecordItemIndex(3));
            repo.Salesforce_Website_Chrome.Asset_REO_Liability.Make_And_Year_In_Automobile_Owned.PressKeys("Honda Civic 2019");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Return}' with focus on 'Salesforce_Website_Chrome.Asset_REO_Liability.Make_And_Year_In_Automobile_Owned'.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.Make_And_Year_In_Automobile_OwnedInfo, new RecordItemIndex(4));
            repo.Salesforce_Website_Chrome.Asset_REO_Liability.Make_And_Year_In_Automobile_Owned.PressKeys("{Return}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Asset_REO_Liability.Automobile_Value' at Center.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.Automobile_ValueInfo, new RecordItemIndex(5));
            repo.Salesforce_Website_Chrome.Asset_REO_Liability.Automobile_Value.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Value to '' on item 'Salesforce_Website_Chrome.Asset_REO_Liability.Automobile_Value'.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.Automobile_ValueInfo, new RecordItemIndex(6));
            repo.Salesforce_Website_Chrome.Asset_REO_Liability.Automobile_Value.Element.SetAttributeValue("Value", "");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '25000' with focus on 'Salesforce_Website_Chrome.Asset_REO_Liability.Automobile_Value'.", repo.Salesforce_Website_Chrome.Asset_REO_Liability.Automobile_ValueInfo, new RecordItemIndex(7));
            repo.Salesforce_Website_Chrome.Asset_REO_Liability.Automobile_Value.PressKeys("25000");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
