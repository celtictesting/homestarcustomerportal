﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarCP.DataEntry
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Enter_Loan_Term_Loan_Details recording.
    /// </summary>
    [TestModule("610a3133-89ee-4872-86ad-32d221347851", ModuleType.Recording, 1)]
    public partial class Enter_Loan_Term_Loan_Details : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarCP.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarCP.HomestarSalesforceRepository repo = global::HomestarCP.HomestarSalesforceRepository.Instance;

        static Enter_Loan_Term_Loan_Details instance = new Enter_Loan_Term_Loan_Details();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Enter_Loan_Term_Loan_Details()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Enter_Loan_Term_Loan_Details Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{PageUp}'.", new RecordItemIndex(0));
            Keyboard.Press("{PageUp}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Invoke action", "Invoking WaitForDocumentLoaded() on item 'Salesforce_Website_Chrome'.", repo.Salesforce_Website_Chrome.SelfInfo, new RecordItemIndex(1));
            repo.Salesforce_Website_Chrome.Self.WaitForDocumentLoaded();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_Details' at Center.", repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_DetailsInfo, new RecordItemIndex(2));
            repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_Details.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '30' with focus on 'Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_Details'.", repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_DetailsInfo, new RecordItemIndex(3));
            repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_Details.PressKeys("30");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Tab}' with focus on 'Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_Details'.", repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_DetailsInfo, new RecordItemIndex(4));
            repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_Details.PressKeys("{Tab}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (InnerText='The number is too low.') on item 'Salesforce_Website_Chrome.Loan_Information.TheNumberIsTooLow'.", repo.Salesforce_Website_Chrome.Loan_Information.TheNumberIsTooLowInfo, new RecordItemIndex(5));
            Validate.AttributeEqual(repo.Salesforce_Website_Chrome.Loan_Information.TheNumberIsTooLowInfo, "InnerText", "The number is too low.");
            Delay.Milliseconds(100);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'Salesforce_Website_Chrome.Loan_Information.Loan_Term_2_In_Loan_Details' at Center.", repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_2_In_Loan_DetailsInfo, new RecordItemIndex(6));
            repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_2_In_Loan_Details.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key 'Ctrl+A' Press with focus on 'Salesforce_Website_Chrome.Loan_Information.Loan_Term_2_In_Loan_Details'.", repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_2_In_Loan_DetailsInfo, new RecordItemIndex(7));
            Keyboard.PrepareFocus(repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_2_In_Loan_Details);
            Keyboard.Press(System.Windows.Forms.Keys.A | System.Windows.Forms.Keys.Control, 30, Keyboard.DefaultKeyPressTime, 1, true);
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Back}' with focus on 'Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_Details'.", repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_DetailsInfo, new RecordItemIndex(8));
            repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_Details.PressKeys("{Back}");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '365' with focus on 'Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_Details'.", repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_DetailsInfo, new RecordItemIndex(9));
            repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_Details.PressKeys("365");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '{Tab}' with focus on 'Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_Details'.", repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_DetailsInfo, new RecordItemIndex(10));
            repo.Salesforce_Website_Chrome.Loan_Information.Loan_Term_In_Loan_Details.PressKeys("{Tab}");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
