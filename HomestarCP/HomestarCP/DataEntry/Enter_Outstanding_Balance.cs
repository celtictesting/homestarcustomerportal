﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarCP.DataEntry
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Enter_Outstanding_Balance recording.
    /// </summary>
    [TestModule("c29f545e-1602-4e56-ac14-e76fb2c92e17", ModuleType.Recording, 1)]
    public partial class Enter_Outstanding_Balance : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarCP.HomestarUserPortalRepository repository.
        /// </summary>
        public static global::HomestarCP.HomestarUserPortalRepository repo = global::HomestarCP.HomestarUserPortalRepository.Instance;

        static Enter_Outstanding_Balance instance = new Enter_Outstanding_Balance();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Enter_Outstanding_Balance()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Enter_Outstanding_Balance Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'New_Application.Liabilities.Outstanding_Balance_Liability_Details' at Center.", repo.New_Application.Liabilities.Outstanding_Balance_Liability_DetailsInfo, new RecordItemIndex(0));
            repo.New_Application.Liabilities.Outstanding_Balance_Liability_Details.Click();
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Set value", "Setting attribute Value to '' on item 'New_Application.Liabilities.Outstanding_Balance_Liability_Details'.", repo.New_Application.Liabilities.Outstanding_Balance_Liability_DetailsInfo, new RecordItemIndex(1));
            repo.New_Application.Liabilities.Outstanding_Balance_Liability_Details.Element.SetAttributeValue("Value", "");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence '3000' with focus on 'New_Application.Liabilities.Outstanding_Balance_Liability_Details'.", repo.New_Application.Liabilities.Outstanding_Balance_Liability_DetailsInfo, new RecordItemIndex(2));
            repo.New_Application.Liabilities.Outstanding_Balance_Liability_Details.PressKeys("3000", 20);
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
