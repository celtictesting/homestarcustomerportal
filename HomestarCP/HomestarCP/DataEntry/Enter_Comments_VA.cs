﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarCP.DataEntry
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Enter_Comments_VA recording.
    /// </summary>
    [TestModule("4ab5cb4b-706d-4f5a-b90b-766deed0534b", ModuleType.Recording, 1)]
    public partial class Enter_Comments_VA : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarCP.HomestarSalesforceRepository repository.
        /// </summary>
        public static global::HomestarCP.HomestarSalesforceRepository repo = global::HomestarCP.HomestarSalesforceRepository.Instance;

        static Enter_Comments_VA instance = new Enter_Comments_VA();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Enter_Comments_VA()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Enter_Comments_VA Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'DocuSign.Comments' at Center.", repo.DocuSign.CommentsInfo, new RecordItemIndex(0));
            repo.DocuSign.Comments.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'This is a comment' with focus on 'DocuSign.Comments'.", repo.DocuSign.CommentsInfo, new RecordItemIndex(1));
            repo.DocuSign.Comments.PressKeys("This is a comment");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'DocuSign.Comments_2' at Center.", repo.DocuSign.Comments_2Info, new RecordItemIndex(2));
            repo.DocuSign.Comments_2.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'Another comment' with focus on 'DocuSign.Comments_2'.", repo.DocuSign.Comments_2Info, new RecordItemIndex(3));
            repo.DocuSign.Comments_2.PressKeys("Another comment");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Mouse", "Mouse Left Click item 'DocuSign.Comments_3' at Center.", repo.DocuSign.Comments_3Info, new RecordItemIndex(4));
            repo.DocuSign.Comments_3.Click();
            Delay.Milliseconds(200);
            
            Report.Log(ReportLevel.Info, "Keyboard", "Key sequence 'Another comment' with focus on 'DocuSign.Comments_3'.", repo.DocuSign.Comments_3Info, new RecordItemIndex(5));
            repo.DocuSign.Comments_3.PressKeys("Another comment");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
