﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// Your custom recording code should go in this file.
// The designer will only add methods to this file, so your custom code won't be overwritten.
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Repository;
using Ranorex.Core.Testing;

namespace HomestarCP.SetUp_TearDown
{
    public partial class OpenBrowser
    {
        /// <summary>
        /// This method gets called right after the recording has been started.
        /// It can be used to execute recording specific initialization code.
        /// </summary>
        private void Init()
        {
            // Your recording specific initialization code goes here.
        }

        public void OptionalLogout(RepoItemInfo logoutButton, RepoItemInfo atagInfo, RepoItemInfo buttontagInfo)
        {
        	if (logoutButton.Exists())
        	{
        		Report.Log(ReportLevel.Info, "Mouse", "(Optional Action)\r\nMouse Left Click item 'unknownInfo' at Center.", logoutButton);
            	logoutButton.FindAdapter<Unknown>().Click();
            	
            	Report.Log(ReportLevel.Info, "Mouse", "(Optional Action)\r\nMouse Left Click item 'atagInfo' at Center.", atagInfo);
            	atagInfo.FindAdapter<ATag>().Click();
            	
            	Report.Log(ReportLevel.Info, "Mouse", "(Optional Action)\r\nMouse Left Click item 'buttontagInfo' at Center.", buttontagInfo);
            	buttontagInfo.FindAdapter<ButtonTag>().Click();
            	
            	Report.Info("Logged Out");
        	}
        	
        }

    }
}
