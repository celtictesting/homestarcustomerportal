﻿///////////////////////////////////////////////////////////////////////////////
//
// This file was automatically generated by RANOREX.
// DO NOT MODIFY THIS FILE! It is regenerated by the designer.
// All your modifications will be lost!
// http://www.ranorex.com
//
///////////////////////////////////////////////////////////////////////////////

using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Drawing;
using System.Threading;
using WinForms = System.Windows.Forms;

using Ranorex;
using Ranorex.Core;
using Ranorex.Core.Testing;
using Ranorex.Core.Repository;

namespace HomestarCP.Validation
{
#pragma warning disable 0436 //(CS0436) The type 'type' in 'assembly' conflicts with the imported type 'type2' in 'assembly'. Using the type defined in 'assembly'.
    /// <summary>
    ///The Validate_Address_Populated_CoApp recording.
    /// </summary>
    [TestModule("62bd47dd-a1cc-49ed-9aba-70bc2f02e4c1", ModuleType.Recording, 1)]
    public partial class Validate_Address_Populated_CoApp : ITestModule
    {
        /// <summary>
        /// Holds an instance of the global::HomestarCP.HomestarUserPortalRepository repository.
        /// </summary>
        public static global::HomestarCP.HomestarUserPortalRepository repo = global::HomestarCP.HomestarUserPortalRepository.Instance;

        static Validate_Address_Populated_CoApp instance = new Validate_Address_Populated_CoApp();

        /// <summary>
        /// Constructs a new instance.
        /// </summary>
        public Validate_Address_Populated_CoApp()
        {
        }

        /// <summary>
        /// Gets a static instance of this recording.
        /// </summary>
        public static Validate_Address_Populated_CoApp Instance
        {
            get { return instance; }
        }

#region Variables

#endregion

        /// <summary>
        /// Starts the replay of the static recording <see cref="Instance"/>.
        /// </summary>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        public static void Start()
        {
            TestModuleRunner.Run(Instance);
        }

        /// <summary>
        /// Performs the playback of actions in this recording.
        /// </summary>
        /// <remarks>You should not call this method directly, instead pass the module
        /// instance to the <see cref="TestModuleRunner.Run(ITestModule)"/> method
        /// that will in turn invoke this method.</remarks>
        [System.CodeDom.Compiler.GeneratedCode("Ranorex", global::Ranorex.Core.Constants.CodeGenVersion)]
        void ITestModule.Run()
        {
            Mouse.DefaultMoveTime = 300;
            Keyboard.DefaultKeyPressTime = 100;
            Delay.SpeedFactor = 1.00;

            Init();

            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value='123 Tbd') on item 'New_Application.Get_Started.CoApp_Street_Address'.", repo.New_Application.Get_Started.CoApp_Street_AddressInfo, new RecordItemIndex(0));
            Validate.AttributeEqual(repo.New_Application.Get_Started.CoApp_Street_AddressInfo, "Value", "123 Tbd");
            Delay.Milliseconds(0);
            
            Report.Log(ReportLevel.Info, "Validation", "Validating AttributeEqual (Value='30135') on item 'New_Application.Get_Started.CoApp_Zip_Code'.", repo.New_Application.Get_Started.CoApp_Zip_CodeInfo, new RecordItemIndex(1));
            Validate.AttributeEqual(repo.New_Application.Get_Started.CoApp_Zip_CodeInfo, "Value", "30135");
            Delay.Milliseconds(0);
            
        }

#region Image Feature Data
#endregion
    }
#pragma warning restore 0436
}
